# nullcode - web

A home for all my personal projects, built using svelte.

To deploy:
```bash
./deploy.sh
```
This will build and copy the build directory to the location where it will be mounted by the nginx docker image
