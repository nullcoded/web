#!/usr/bin/env bash

source .secret/env.sh

npm run build &&
echo "rsync -avhH --delete build/* ${DEPLOY_LOCATION}" &&
rsync -avhH --delete build/* ${DEPLOY_LOCATION} &&
echo "done."
